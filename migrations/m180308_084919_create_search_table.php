<?php

use yii\db\Migration;

/**
 * Handles the creation of table `search`.
 */
class m180308_084919_create_search_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('search', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'thing' => $this->string(),
            'ot' => $this->date(),
            'dot' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('search');
    }
}
