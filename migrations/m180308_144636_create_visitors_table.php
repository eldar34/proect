<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visitors`.
 */
class m180308_144636_create_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('visitors', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'mail' => $this->string(),
            'phone' => $this->string(),
            'thing' => $this->string(),
            'date' => $this->date()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('visitors');
    }
}
