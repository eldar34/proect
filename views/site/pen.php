<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MyList;
?>
    <h1>Create Post</h1>
<?php if(Yii::$app->session->getFlash('status')):?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('status'); ?>
    </div>
<?php endif;?>


<?php $form = ActiveForm::begin(); ?>

    <div class="row"  style="display:flex; flex-direction:column;">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'mail')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone')
                ->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
            ]);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'thing')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= Html::submitButton('Create', ['class'=>'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>