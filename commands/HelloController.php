<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\User;
use app\models\Visitors;
use app\models\SearchSave;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionSeeds()
    {
        // Check existing user
        $userexist=User::find()->where([ 'id' => 1 ])->exists();

        // Create faker
        $faker = \Faker\Factory::create();

        if($userexist){
            $user=User::findOne(1);
            $useremail = $user->email;
            $userpassword = $user->password;
        }else{
            
            $useremail = '';
            $userpassword = strtolower($faker->lastName);

            $user = new User();
            $user->name = $faker->firstName;
            $user->email = $faker->email;
            $user->password = $userpassword;
            $user->isAdmin = 1;
            $user->photo = $faker->lastName;
            if ( $user->save()){
                    $useremail = $user->email;
                }
        }
          
        for ( $i = 1; $i <= 100; $i++ )
        {
            
            $visitor = new Visitors();
            $visitor->name = $faker->firstName;
            $visitor->phone = $faker->phoneNumber;
            $visitor->mail = $faker->email;
            $visitor->thing = $faker->lastName;
            $visitor->date = $faker->date('Y-m-d');

            if(!$visitor->save()){
                $visitor->validate();
                var_dump($visitor->errors);
                break;
            }else{
                if($i == 1){
                    // If doesn't exist records, create new
                    $searchexists=SearchSave::find()->where([ 'id' => 1 ])->exists();

                    if(!$searchexists){
                        $searchseve = new SearchSave();
                        $searchseve->name = $visitor->name;
                        $searchseve->thing = $visitor->thing;
                        $searchseve->thing = $visitor->thing;
                        $searchseve->ot = $visitor->date;
                        $searchseve->dot = $visitor->date;
                        $searchseve->save();
                    }            
                }
            }
        }

        echo "email: \n";
        echo $useremail . "\n";
        echo "password: \n";
        echo $userpassword . "\n";      
        
    }
}
