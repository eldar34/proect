<?php

namespace app\models;


class MyList extends \yii\db\ActiveRecord{


    public function rules()
    {
        return [
            // атрибут required указывает, что name, email, subject, body обязательны для заполнения
            [['mail', 'thing', 'name', 'phone'], 'required'],
            ['mail', 'email'],
            ['mail', 'unique'],
            [['name', 'thing',],'string'],
            [['name', 'thing', 'phone'], 'trim'],
            ['name', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9ё\s,]+$/u'],
            ['thing', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9ё\s,]+$/u'],
            ['phone', 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/'],
            ['name', 'string', 'length' => [2, 12]],
            ['thing', 'string', 'length' => [2, 12]],
            [['date'], 'date', 'format'=>'php:Y-m-d'],
            [['date'], 'default', 'value' => date('Y-m-d')],

        ];
    }

    public static function tableName(){


        return 'visitors';
    }

    public static function getAll(){

        $data = self::find()->all();
        return $data;
    }

    public static function getOne($id){

        $data = self::find()
            ->where(['id'=>$id])
            ->one();
        return $data;

    }

    public function addPost($params)
    {
        $addParams = new MyList();
        $addParams->name = $params->name;

    }


}

?>