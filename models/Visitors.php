<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitors".
 *
 * @property int $id
 * @property string $name
 * @property string $mail
 * @property int $phone
 * @property string $thing
 * @property string $date
 */
class Visitors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visitors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'mail', 'phone', 'thing', 'date'], 'required'],
            [['phone'], 'string'],
            [['date'], 'safe'],
            [['name', 'mail', 'thing'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mail' => 'Mail',
            'phone' => 'Phone',
            'thing' => 'Thing',
            'date' => 'Date',
        ];
    }
}
