<?php
namespace app\models;
use yii\data\Pagination;


class FixSearch extends \yii\db\ActiveRecord{



    public function rules()
    {
        return [
            [['name'], 'required'],
            ];
    }

    public static function tableName(){


        return 'visitors';
    }

    public static function getAll(){

        $data = self::find()->all();
        return $data;
    }

    /*

    public function search($params)
    {
        $today = $params['FixSearch']['name'];
        $fixed = explode(" ", $today);

        return $fixed;

    }
    */

    public static function getAllin($params){
       $today = $params['FixSearch']['name'];
        $fixed = explode(" ", $today);
        $data = self::find()
            ->where([
                'name'=>$fixed[0],
                'thing'=>$fixed[1],
            ])
            ->all();

        return $data;
    }

    public function search($inform) {




        $from = $inform['FixSearch']['date'];
        $to = $inform['FixSearch']['phone'];



        $query = $this->find()
            ->andFilterWhere(['>=', 'date', $from])
            ->andFilterWhere(['<=', 'date', $to]);
        /*
        if ($this->validate()) {
            $query
                ->andFilterWhere(['>=', 'date', $this->fromDate])
                ->andFilterWhere(['<=', 'date', $this->toDate]);
        }
        */

        // build a DB query to get all articles


        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>2]);

        // limit the query using the pagination and retrieve the articles
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $data['articles'] = $articles;
        $data['pagination'] = $pagination;

        return $data;

    }







}
?>