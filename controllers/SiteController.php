<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
/*
 * Импортирум класс MyList
 */
use app\models\MyList;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*
         * Cоздаём экземпляр класса MyList
         */
        $model = new MyList();

        /*
         * Если глобальный массив $_POST не пустой
         * То записываем значения из формы в БД
         */
        if ($model->load(Yii::$app->request->post())) {
            $model->name=$_POST['MyList']['name'];
            $model->mail = $_POST['MyList']['mail'];
            $model->phone = $_POST['MyList']['phone'];
            $model->thing = $_POST['MyList']['thing'];
            //$model->attributes = $_POST['MyList'];
            /*
             * Если данные прошли валидацию и записались в БД
             * То выводим надпись что запись добавлена в БД
             */
            if($model->validate() && $model->save()){
                Yii::$app->getSession()->setFlash('status', 'Your post has been added!');
                return $this->redirect(['index']);
            }
        }
        return $this->render('pen', ['model'=>$model,]);
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */

}
