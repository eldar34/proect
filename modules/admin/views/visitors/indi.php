<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\VisitorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visitors';
$this->params['breadcrumbs'][] = $this->title;
?>



<?php $form = ActiveForm::begin(['method' => 'post',]); ?>
<div class="row"  style="display:flex; flex-direction:column;">
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput()->label('Name Thing')
            ->hint('For example: Amelia abatable')?>
    </div>

    <div class="col-md-12">
        <?= Html::submitButton('Search', ['class'=>'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="visitors-index"><br>


    <?php Pjax::begin(); ?>


    <?=Html::tag('br', Html::encode('hello'), ['class' => 'visitors-index'])?>
    <?=Html::tag('div', Html::encode(' '), ['class' => 'row'])?>
    <?=Html::tag('br', Html::encode('hello'), ['class' => 'visitors-index'])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'mail',
            'phone',
            'thing',
            'date',
            //'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
