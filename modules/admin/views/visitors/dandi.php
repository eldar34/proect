<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\widgets\Pjax;


?>


<?php
//echo var_dump($dataProvider);
// var_dump($session['mistic']);
?>

<?php $form = ActiveForm::begin(); ?>
<div class="col-md-8 row"  style="display:flex; flex-direction:row; justify-content: space-around;">
<?= $form->field($model, 'date')->label('From')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>

<?= $form->field($model, 'phone')->label('To')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>
</div>


<div class="col-md-12"><br>
    <?= Html::submitButton('Search', ['class'=>'btn btn-success']) ?>
    <?=Html::tag('br', Html::encode(' '), ['class' => 'visitors-index'])?>
</div><br>
<?php ActiveForm::end(); ?>



<div class="visitors-index"><br>


<?php Pjax::begin(); ?>


    <?=Html::tag('br', Html::encode('hello'), ['class' => 'visitors-index'])?>
    <?=Html::tag('div', Html::encode(' '), ['class' => 'row'])?>
    <?=Html::tag('br', Html::encode('hello'), ['class' => 'visitors-index'])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'mail',
            'phone',
            'thing',
            'date',
            //'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>




<?php /*
\yii\widgets\Pjax::begin();

echo Html::beginForm();
echo DatePicker::widget([
'name' => 'from_date',
'type' => DatePicker::TYPE_RANGE,
'name2' => 'to_date',
'pluginOptions' => [
'autoclose'=>true,
'format' => 'dd-mm-yyyy'
]
]);
echo Html::submitButton();
echo Html::endForm();
*/ ?>
