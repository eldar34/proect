<?php

use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\grid\GridView;
?>


<?php if(isset($dataProvider)){ ?>

    <table class="table">
        <thead>
        <tr>
            <td>#</td>
            <td>Name</td>
            <td>Mail</td>
            <td>Phone</td>
            <td>Thing</td>
            <td>Date</td>
            <td>ToDo</td>
        </tr>
        </thead>

        <tbody>

        <?php foreach ($dataProvider as $item): ?>
            <tr>
                <td><?=$item->id?></td>
                <td><?=$item->name?></td>
                <td><?=$item->mail?></td>
                <td><?=$item->phone?></td>
                <td><?=$item->thing?></td>
                <td><?=$item->date?></td>
                <td>
                    <p>Hello</p>
                    <?php // echo Html::a('Редактировать', ['/admin/default/edit/','id'=>$item->id] )?>
                    <php // Html::a('Редактировать', ['edit', 'id'=>$item->id]) ?>
                    <?php // echo Html::a('Удалить', ['/site/default/delete/','id'=>$item->id] )?>
                    <php //Html::a('Удалить', ['delete', 'id'=>$item->id]) ?>
                </td>
            </tr>
        <?php endforeach; ?>


        </tbody>

    </table>

    <?php

    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);

    ?>

<?php } ?>



<div class="visitors-index">



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'mail',
            'phone',
            'thing',
            'date',
            //'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>







