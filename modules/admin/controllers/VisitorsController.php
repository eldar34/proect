<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Visitors;
use app\models\VisitorsSearch;
use app\models\FixSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MyList;
use app\models\SearchSave;

/**
 * VisitorsController implements the CRUD actions for Visitors model.
 */
class VisitorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Visitors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisitorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Visitors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionIndi()
    {
        $model = new VisitorsSearch();
        $searchModel = new VisitorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post()) {
            $myvar = SearchSave::updn(Yii::$app->request->post());

            $name = $myvar->name;
            $thing = $myvar->thing;

            if($thing==''){

                $model = new VisitorsSearch();
                $searchModel = new VisitorsSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query
                    ->andFilterWhere(['=', 'name', $name]);

                return $this->render('indi', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);

            }

            $model = new VisitorsSearch();
            $searchModel = new VisitorsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->andFilterWhere(['=', 'name', $name])
                ->andFilterWhere(['=', 'thing', $thing]);

            return $this->render('indi', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);


        }else {

            $myfix = SearchSave::findOne(['id' => 1]);

            $name = $myfix->name;
            $thing = $myfix->thing;

            if($thing == ''){

                $model = new VisitorsSearch();
                $searchModel = new VisitorsSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query
                    ->andFilterWhere(['=', 'name', $name]);

                return $this->render('indi', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);

                die;


            }

            $model = new VisitorsSearch();
            $searchModel = new VisitorsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->andFilterWhere(['=', 'name', $name])
                ->andFilterWhere(['=', 'thing', $thing]);


            return $this->render('indi', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('indi', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }





    public function actionDandi()
    {



        $from;
        $to;

        $model = new VisitorsSearch();
        $searchModel = new VisitorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        if(Yii::$app->request->post()){

            $myfix = SearchSave::upd(Yii::$app->request->post());

            $from = $myfix->ot;
            $to = $myfix->dot;



            $model = new VisitorsSearch();
            $searchModel = new VisitorsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query
                    ->andFilterWhere(['>=', 'date', $from])
                    ->andFilterWhere(['<=', 'date', $to]);



            return $this->render('dandi', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,


            ]);



        }else{


            $myfix = SearchSave::findOne(['id' => 1]);

            $from = $myfix->ot;
            $to = $myfix->dot;

            if($to == '0000-00-00'){




            $model = new VisitorsSearch();
            $searchModel = new VisitorsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->andFilterWhere(['>=', 'date', $from]);

            }elseif($from == '0000-00-00'){

                $model = new VisitorsSearch();
                $searchModel = new VisitorsSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query
                    ->andFilterWhere(['<=', 'date', $to]);

            }elseif($from !== '0000-00-00' && $to !== '0000-00-00' ){

                $model = new VisitorsSearch();
                $searchModel = new VisitorsSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query
                    ->andFilterWhere(['>=', 'date', $from])
                    ->andFilterWhere(['<=', 'date', $to]);

            }








        return $this->render('dandi', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);

        }
    }

    public function actionResult()
    {
/*
            $model = new FixSearch();

        if(Yii::$app->request->post()) {

            $myarray = $model->search(Yii::$app->request->post());


            return $this->render('dandi', [
                'model'=>$model,
                'dataProvider' => $myarray['articles'],
                'pagination'=>$myarray['pagination'],
            ]);
        }
            else{



            }
*/

    }

    /**
     * Creates a new Visitors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Visitors();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Visitors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Visitors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visitors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visitors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visitors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>